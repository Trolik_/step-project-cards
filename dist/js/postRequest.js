import checkCard from "./checkAndRenderClass.js";
import visitsArray from "./index.js";

const postRequest = async (
	doctorName,
	searchDoctor,
	name,
	purpose,
	description,
	urgency,
	lastVisit,
	ageTherapist,
	ageCardiologist,
	pressure,
	massIndex,
	diseases,
	status
) => {
	if (doctorName === "Dentist") {
		const receiveData = await fetch(
			"https://ajax.test-danit.com/api/v2/cards",
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					Authorization: `${localStorage.getItem("Authorization")}`,
				},
				body: JSON.stringify({
					doctor: `${doctorName}`,
					name: `${name}`,
					purpose: `${purpose}`,
					description: `${description}`,
					urgency: `${urgency}`,
					lastVisit: `${lastVisit}`,
					status: "Відчинено",
				}),
			}
		).then((response) => response.json());
		console.log(receiveData);
		checkCard("Dentist", receiveData);
		if (visitsArray.length === 0) {
			document.querySelector(".header__loginBtn").innerText = "Create Visit";
			document.querySelector(
				".card__container"
			).innerHTML = `<h2 class="empty-card" style='color: aliceblue; line-height: 40px'>Не знайдено результатів</h2>`;
		} else {
			const err = document.querySelector(".empty-card")?.remove();
		}
		document.querySelector(".modal-background").remove();
	} else if (doctorName === "Therapist") {
		console.log(ageTherapist);
		const receiveData = await fetch(
			"https://ajax.test-danit.com/api/v2/cards",
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					Authorization: `${localStorage.getItem("Authorization")}`,
				},
				body: JSON.stringify({
					doctor: `${doctorName}`,
					name: `${name}`,
					purpose: `${purpose}`,
					description: `${description}`,
					urgency: `${urgency}`,
					ageTherapist: `${ageTherapist}`,
					status: "Відчинено",
				}),
			}
		).then((response) => response.json());
		console.log(receiveData);
		checkCard("Therapist", receiveData);
		if (visitsArray.length === 0) {
			document.querySelector(".header__loginBtn").innerText = "Create Visit";
			document.querySelector(
				".card__container"
			).innerHTML = `<h2 class="empty-card" style='color: aliceblue; line-height: 40px'>Не знайдено результатів</h2>`;
		} else {
			const err = document.querySelector(".empty-card")?.remove();
		}
		document.querySelector(".modal-background").remove();
	} else if (doctorName === "Cardiologist") {
		const receiveData = await fetch(
			"https://ajax.test-danit.com/api/v2/cards",
			{
				method: "POST",
				headers: {
					"Content-Type": "application/json",
					Authorization: `${localStorage.getItem("Authorization")}`,
				},
				body: JSON.stringify({
					doctor: `${doctorName}`,
					name: `${name}`,
					purpose: `${purpose}`,
					description: `${description}`,
					urgency: `${urgency}`,
					ageCardiologist: `${ageCardiologist}`,
					pressure: `${pressure}`,
					massIndex: `${massIndex}`,
					diseases: `${diseases}`,
					status: "Відчинено",
				}),
			}
		).then((response) => response.json());
		document.querySelector(".modal-background").remove();
		checkCard("Cardiologist", receiveData);
		if (visitsArray.length === 0) {
			document.querySelector(".header__loginBtn").innerText = "Create Visit";
			document.querySelector(
				".card__container"
			).innerHTML = `<h2 class="empty-card" style='color: aliceblue; line-height: 40px'>Не знайдено результатів</h2>`;
		} else {
			const err = document.querySelector(".empty-card")?.remove();
		}
	} else {
		console.log("error post information");
	}
};
export default postRequest;
