import VisitCards from "./renderVisitCards.js";

export class VisitCardsCardiologist extends VisitCards {
	constructor(
		name,
		doctor,
		description,
		urgency,
		purpose,
		status,
		id,
		display = "none",
		showMoreBtnText = "Показати більше",
		age,
		pressure,
		massIndex,
		diseases
	) {
		super(
			name,
			doctor,
			description,
			urgency,
			purpose,
			status,
			id,
			display,
			showMoreBtnText
		);
		this.age = age;
		this.pressure = pressure;
		this.massIndex = massIndex;
		this.diseases = diseases;
	}

	createElements() {
		super.createElements();
		this.moreInfo.insertAdjacentHTML(
			"afterbegin",
			`
      <p>Вік: ${this.age}</p>
      <p>Тиск: ${this.pressure}</p>
      <p>Індекс маси тіла: ${this.massIndex}</p>
      <p>Хвороби: ${this.diseases}</p>

      `
		);
	}

	render() {
		super.render();
	}
}


export class VisitCardsDentist extends VisitCards {
	constructor(
		name,
		doctor,
		description,
		urgency,
		purpose,
		status,
		id,
		display = "none",
		showMoreBtnText = "Показати більше",
		lastVisit
	) {
		super(
			name,
			doctor,
			description,
			urgency,
			purpose,
			status,
			id,
			display,
			showMoreBtnText
		);
		this.lastVisit = lastVisit;
	}

	createElements() {
		super.createElements();
		this.moreInfo.insertAdjacentHTML(
			"afterbegin",
			`
    <p>Остання дата візиту: ${this.lastVisit}</p>
    `
		);
	}

	render() {
		super.render();
	}
}


export class VisitCardsTherapist extends VisitCards {
	constructor(
		name,
		doctor,
		description,
		urgency,
		purpose,
		status,
		id,
		display = "none",
		showMoreBtnText = "Показати більше",
		age
	) {
		super(
			name,
			doctor,
			description,
			urgency,
			purpose,
			status,
			id,
			display,
			showMoreBtnText
		);
		this.age = age;
	}

	createElements() {
		super.createElements();
		this.moreInfo.insertAdjacentHTML(
			"afterbegin",
			`
    <p>Вік: ${this.age}</p>
    `
		);
	}

	render() {
		super.render();
	}
}


const checkAndFilterCard = (doctor, element, display) => {
	if (display === "none") {
		if (doctor === "Cardiologist") {
			const {
				name,
				doctor,
				description,
				priority,
				visitPurpose,
				status,
				id,
				age,
				pressure,
				massIndex,
				diseases,
			} = element;
			const newElement = new VisitCardsCardiologist(
				name,
				doctor,
				description,
				priority,
				visitPurpose,
				status,
				id,
				"none",
				"Показати більше",
				age,
				pressure,
				massIndex,
				diseases
			);
			newElement.render();
		} else if (doctor === "Dentist") {
			const {
				name,
				doctor,
				description,
				priority,
				visitPurpose,
				status,
				id,
				lastVisit,
			} = element;
			const newElement = new VisitCardsDentist(
				name,
				doctor,
				description,
				priority,
				visitPurpose,
				status,
				id,
				"none",
				"Показати більше",
				lastVisit
			);
			newElement.render();
		} else {
			const {
				name,
				doctor,
				description,
				priority,
				visitPurpose,
				status,
				id,
				age,
			} = element;
			const newElement = new VisitCardsTherapist(
				name,
				doctor,
				description,
				priority,
				visitPurpose,
				status,
				id,
				"none",
				"Показати більше",
				age
			);
			newElement.render();
		}
	} else {
		if (doctor === "Cardiologist") {
			const {
				name,
				doctor,
				description,
				priority,
				visitPurpose,
				status,
				id,
				age,
				pressure,
				massIndex,
				diseases,
			} = element;
			const newElement = new VisitCardsCardiologist(
				name,
				doctor,
				description,
				priority,
				visitPurpose,
				status,
				id,
				"block",
				"Показати менше",
				age,
				pressure,
				massIndex,
				diseases
			);
			newElement.render();
		} else if (doctor === "Dentist") {
			const {
				name,
				doctor,
				description,
				priority,
				visitPurpose,
				status,
				id,
				lastVisit,
			} = element;
			const newElement = new VisitCardsDentist(
				name,
				doctor,
				description,
				priority,
				visitPurpose,
				status,
				id,
				"block",
				"Показати менше",
				lastVisit
			);
			newElement.render();
		} else {
			const {
				name,
				doctor,
				description,
				priority,
				visitPurpose,
				status,
				id,
				age,
			} = element;
			const newElement = new VisitCardsTherapist(
				name,
				doctor,
				description,
				priority,
				visitPurpose,
				status,
				id,
				"block",
				"Показати менше",
				age
			);
			newElement.render();
		}
	}
};

export default checkAndFilterCard;