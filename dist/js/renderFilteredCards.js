import checkAndFilterCard from "./checkAndFilterCards.js";

let renderFilteredCards = (array) => {
	array.forEach((element) => {
		const {doctor, display} = element;
		checkAndFilterCard(doctor, element, display);
	});
};

export default renderFilteredCards;
