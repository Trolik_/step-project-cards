"use strict";

import getServerData from "./loginAuthorization.js";
import openLoginModal from "./openLoginModal.js";
import renderSearchFilters from "./renderFilters.js";

const logInBtn = document.querySelector(".header__loginBtn");

let visitsArray = [];

export default visitsArray;

logInBtn.addEventListener("click", openLoginModal);

document.addEventListener("DOMContentLoaded", () => {
	if (localStorage.Authorization) {
		getServerData();
		renderSearchFilters();
	} else {
		return;
	}
});

