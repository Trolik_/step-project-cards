import filterArr from "./filtrationOfCardsByParam.js";
import visitsArray from "./index.js";

const renderSearchFilters = () => {
	const container = document.querySelector(".main__wrapper");
	const filterContainer = document.createElement("div");
	filterContainer.classList.add("filter__container");
	const searchInput = document.createElement("input");
	const searchDropMenu = document.createElement("input");
	const formStatus = document.createElement("form");
	const formPriority = document.createElement("form");
	formStatus.insertAdjacentHTML(
		"afterbegin",
		`
  <label>Оберіть статус</label>
<select>
  <option selected>Статус</option>
  <option>Відчинено</option>
  <option>Зачинено</option>
</select>
  `
	);
	formPriority.insertAdjacentHTML(
		"afterbegin",
		`
    <label>Оберіть пріоритет</label>
  <select>
  <option selected>Пріоритетність</option>
  <option>Невідкладна</option>
  <option>Пріоритетна</option>
  <option>Звичайна</option>
</select>
  `
	);

	formStatus.addEventListener("change", async (e) => {
		filterArr(
			visitsArray,
			e.target.closest(".filter__container").children[1].value,
			e.target.closest(".filter__container").children[2].children[1].value,
			e.target.value
		);
	});
	formPriority.addEventListener("change", (e) => {
		filterArr(
			visitsArray,
			e.target.parentNode.previousSibling.value,
			e.target.value,
			e.target.parentNode.nextSibling.children[1].value
		);
	});
	searchInput.addEventListener("input", (e) => {
		filterArr(
			visitsArray,
			e.target.value,
			e.target.nextSibling.children[1].value,
			e.target.nextSibling.nextSibling.children[1].value
		);
	});
	const header = document.createElement("h1");
	header.innerText = "Пошук картки пацієнта";
	filterContainer.prepend(header, searchInput, formPriority, formStatus);
	container.prepend(filterContainer);
};

export default renderSearchFilters;
