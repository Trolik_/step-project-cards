import ModalAdditionalDataChange, {visitCardiologist, visitDentist, visitTherapist} from "./modalAdditionalDataChange.js";

const distinguishDoctorFromOptions = (targetValue, currentModal) => {
	if (targetValue === "1") {
		currentModal.deleteModal();
		new visitDentist().render();
	} else if (targetValue === "2") {
		currentModal.deleteModal();
		new visitTherapist().render();
	} else if (targetValue === "3") {
		currentModal.deleteModal();
		new visitCardiologist().render();
	} else {
		currentModal.deleteModal();
		new ModalAdditionalDataChange().render();
	}
};
export default distinguishDoctorFromOptions;
