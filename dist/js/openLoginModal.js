import BaseModal from "./baseModal.js";

const openLoginModal = () => {
	new BaseModal().render();
};

export default openLoginModal;
