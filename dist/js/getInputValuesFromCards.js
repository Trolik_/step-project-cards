import checkFields from "./checkForRequiredFields.js";
import postRequest from "./postRequest.js";

const getInputValuesFromCards = () => {
	const searchDoctor = document.querySelector("#select-doctor")?.value;
	const searchDoctorText = document.querySelector("#select-doctor");
	const doctorName = searchDoctorText.querySelector("[selected]").textContent;
	const doctor = searchDoctorText.querySelector("[selected]").value;
	const name = document.querySelector(".form-control")?.value;
	const purpose = document.querySelector("#inputWorriesDentist")?.value;
	const description = document.querySelector("#inputDescriptionDentist")?.value;
	const urgency = document.querySelector("#inputUrgencyDentist")?.value;
	const lastVisit = document.querySelector("#inputLastVisitDentist")?.value;
	const ageTherapist = document.querySelector("#inputAgeTherapist")?.value;
	const ageCardiologist = document.querySelector(
		"#inputAgeCardiologist"
	)?.value;
	const pressure = document.querySelector("#inputPressureCardiologist")?.value;
	const massIndex = document.querySelector("#inputIndexCardiologist")?.value;
	const diseases = document.querySelector("#inputDiseasesCardiologist")?.value;
	const status = "Відчинено";

	const checkInputs = checkFields(
		doctor,
		name,
		purpose,
		description,
		lastVisit,
		ageCardiologist,
		ageTherapist,
		pressure,
		massIndex,
		diseases
	);

	if (checkInputs) {
		postRequest(
			doctorName,
			searchDoctor,
			name,
			purpose,
			description,
			urgency,
			lastVisit,
			ageTherapist,
			ageCardiologist,
			pressure,
			massIndex,
			diseases,
			status
		);
	} else {
		document.querySelector(".requiredFields__wrongSubmit")?.remove();
		const errContainer = document.querySelector(".modal").firstChild;
		errContainer.insertAdjacentHTML(
			"beforeend",
			`
      <div class="requiredFields__wrongSubmit"> Будь ласка, запоніть всі поля перед відправкою </div>
      `
		);
	}
};

export default getInputValuesFromCards;
