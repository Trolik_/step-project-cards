import renderFilteredCards from "./renderFilteredCards.js";

const container = document.querySelector(".card__container");
const filterArr = async (
	dataArray,
	filterByValue,
	priorityValue,
	statusValue
) => {
	let filteredArr = await dataArray.filter(({name, doctor}) => {
		return (
			name?.toLowerCase().includes(filterByValue.toLowerCase()) ||
			doctor?.toLowerCase().includes(filterByValue.toLowerCase())
		);
	});

	console.log(filteredArr);
	const filterByPrior = filteredArr.filter((el) => {
		const {priority} = el;
		if (priorityValue === "Пріоритетність") {
			return true;
		} else return priorityValue !== "Пріоритетність" && priorityValue === priority;
	});

	const filterByStatus = filterByPrior.filter((el) => {
		const {status} = el;
		if (statusValue === "Статус") {
			return true;
		} else return statusValue !== "Статус" && statusValue === status;
	});

	if (filteredArr.length !== 0) {
		container.innerHTML = "";
		renderFilteredCards(filterByStatus);

		return filteredArr;
	} else {
		container.innerHTML = "<h2 style='color: aliceblue; line-height: 40px'>Жодної картки не знайдено</h2>";
		return filteredArr;
	}
};

export default filterArr;
