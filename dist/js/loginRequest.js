import getServerData from "./loginAuthorization.js";
import renderSearchFilters from "./renderFilters.js";

const userLogin = async (email, password) => {
	const token = await fetch("https://ajax.test-danit.com/api/v2/cards/login", {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
		},
		body: JSON.stringify({email: `${email}`, password: `${password}`}),
	}).then((res) => checkResStatus(res));
	if (token) {
		localStorage.setItem("Authorization", `Bearer ${token}`);
		document.querySelector(".login__modalBg").remove();

		getServerData();
		renderSearchFilters();
	} else {
		document.querySelector(".login__error")?.remove();
		document.querySelector(".login__modalForm").insertAdjacentHTML(
			"beforeend",
			`
    <div class="login__error" style="color:red">Не вірно введено логін або пароль</div>
    `
		);
	}
};

const checkResStatus = (status) => {
	if (status.ok) {
		return status.text();
	} else {

	}
};

export default userLogin;
