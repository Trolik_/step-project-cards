import ModalAdditionalDataChange from "./modalAdditionalDataChange.js";
import checkCard from "./checkAndRenderClass.js";
import openLoginModal from "./openLoginModal.js";

const getServerData = async () => {
	const getCards = await fetch("https://ajax.test-danit.com/api/v2/cards/", {
		headers: {
			"Content-type": "application/json",
			Authorization: `${localStorage.getItem("Authorization")}`,
		},
	}).then((res) => res.text());
	const logInBtn = document.querySelector(".header__loginBtn");
	logInBtn.removeEventListener("click", openLoginModal);
	logInBtn.addEventListener("click", () => {
		new ModalAdditionalDataChange().render();
	});
	if (JSON.parse(getCards).length === 0) {
		document.querySelector(".header__loginBtn").innerText = "Створити візит";
		document.querySelector(
			".card__container"
		).innerHTML = `<h2 class="empty-card" style="color: aliceblue; line-height: 40px">Жодної картки не додано</h2>`;
	} else {
		document.querySelector(".header__loginBtn").innerText = "Створити візит";

		JSON.parse(getCards).forEach((element) => {
			const {doctor} = element;
			checkCard(doctor, element);
		});
	}
};

export default getServerData;
