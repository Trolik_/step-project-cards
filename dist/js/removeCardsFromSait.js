import visitsArray from "./index.js";

const removeCardsFromSait = async (id, cardWrapper) => {
	const authorization = localStorage.getItem("Authorization");

	await fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
		method: "DELETE",
		headers: {
			Authorization: authorization,
		},
	});

	localStorage.setItem("Authorization", authorization);
	cardWrapper.remove();

	const index = visitsArray.findIndex((el) => el.id === id);
	if (index !== -1) {
		visitsArray.splice(index, 1);
	}

	if (visitsArray.length === 0) {
		document.querySelector(".header__loginBtn").innerText = "Створити візит";
		document.querySelector(
			".card__container"
		).innerHTML = `<h2 class="empty-card" style="color: aliceblue; line-height: 40px">Жодної картки не додано</h2>`;
	}
};

export default removeCardsFromSait;
